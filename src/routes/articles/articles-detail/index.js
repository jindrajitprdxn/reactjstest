export const articlesDetail = {
	'path': ':id',
	getComponent(location,cb) {
		require.ensure([],(require) => {
			cb(null, require('../../../components/articles/articles-detail').default)
		},'article-detail')
	}
}
import { articlesDetail } from './articles-detail'
export const articles = {
	'path': 'articles',
	getComponent(location,cb) {
		require.ensure([],(require) => {
			cb(null, require('../../components/articles').default)
		},'question-paper')
	},
	'childRoutes': [
		articlesDetail
	]
}
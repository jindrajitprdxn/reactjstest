import App from '../App.js';
import { Home } from './home';
import { worldMap } from './world-map';
import { questionPaper } from './question-paper';
import { articles } from './articles';
const routes = {
	'path': '/',
	'component': App,
	'indexRoute': Home,

	'childRoutes': [
		worldMap,
		questionPaper,
		articles
	]
}

export default routes;
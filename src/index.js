import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import store from './reducers';
import routes from './routes';
import { Provider } from 'react-redux';
import { Router, browserHistory} from 'react-router';

ReactDOM.render( 
	<Provider store={store}>
    <Router history={browserHistory} routes={routes} />
  </Provider>,
  document.getElementById('root'));
registerServiceWorker();

import React, { Component } from 'react';
import { Link } from 'react-router';
import { Field, reduxForm, reset } from 'redux-form';
import { connect } from 'react-redux';
import dataJson from './data.json';
import BarChat from '../common/bar-chat'
// import "./styles/style.css";
 	class questionPaper  extends Component {
		constructor(props) {
			super(props);
			this.onSubmit = this.onSubmit.bind(this);
			this.generateForm = this.generateForm.bind(this);
			this.rquiredFieldValidation = this.rquiredFieldValidation.bind(this);
			this.submissionResult = this.submissionResult.bind(this);
			this.onClickClearButton = this.onClickClearButton.bind(this);
			this.state = {
				'dataJson': dataJson,
				'showResult': false,
				'resultData': [],
				'resCount': null,
				'totalCountQes': null
			}
		}

		generateForm(data) {
			let count = 0;
			let formStructure = dataJson ? dataJson.map((data, index) => {
				count++;
				return (
					<div  className={`question-container`} key={index}>
		        <label>{`${data.question} *`}</label>
		        {
		        data.options.map((option,index) => {
		        	return (
		        		<div key={index}>
				          <label>
				          <Field
			              name={`option-${count}`}
			              component="input"
			              type="radio"
			              value={option.optionKey}
			              id="radio"
			            /> {`${option.optionData}`}</label>
			          </div>
		        	)
		        })
		      }
		      </div>
				)
			}) : null
			return formStructure
		}
		
		/*required field validation*/
		rquiredFieldValidation() {
			let fields =	document.querySelectorAll('.question-container');
			fields = [].slice.call(fields);
			let questionsCount = 0;
			let requiredValidation = 0;
			/*Delete errors if any*/
			let deleteErrors = 	document.getElementsByClassName('error');
			while(deleteErrors.length > 0) {
				for(let i = 0 ; i < deleteErrors.length; i++) {
					deleteErrors[i].parentNode.removeChild(deleteErrors[i]);
				}
			}
			fields ? fields.map(field => {
				questionsCount++;
				let span = document.createElement('span');
				span.className = "error";
				span.innerText = "";
				let count = 0;
				let inputFields = field.getElementsByTagName('INPUT');
				inputFields = [].slice.call(inputFields);
				inputFields.map(ele => {
					if(ele.checked) {
						count++;
					}
				})
				if(count === 0) {
					span.innerText = "This field is manditory";
					field.appendChild(span)
				} else {
					requiredValidation++;
				}
			}) : null
			if(questionsCount === requiredValidation) {
				return true;
			}
		}

		/*Function for find submission result*/
		submissionResult(data) {
			let dataJson = this.state.dataJson;
			let resultKeys = Object.keys(data);
			let resultData = [];
			let resCount = 0;
			let totalCount = 0;
			resultKeys.map(key => {
				totalCount++;
				let num = parseInt(key.replace('option-',""))
				if(data[key] === dataJson[num - 1].currectAns) {
					resCount++
					let dataObject = {
						'text': `que-${num}`,
						'value': 1
					}
					resultData.push(dataObject)
				} else {
					let dataObject = {
						'text': `que-${num}`,
						'value': 0
					}

					resultData.push(dataObject)
				}
			})

			this.setState({
				'resultData': resultData,
				'showResult': true,
				'resCount': resCount,
				'totalCountQes': totalCount
			})
		}

		onClickClearButton(reset) {
			this.setState({
				'showResult': false
			})
		}
		/* Call function on click submit button*/
		onSubmit(dataJson) {
			let requiredValidation = this.rquiredFieldValidation();
			if(requiredValidation) {
				this.submissionResult(dataJson);
			}
		}
	 	render() {
	 		const {
	 			handleSubmit,
	 			reset
	 		} = this.props;
	 		return(
	 		<div className="world-map">
	 			{/* form body */}
        <form className="question-paper-form" onSubmit={handleSubmit(this.onSubmit.bind(this))}>
            <p className="form-heading" id="form-title">Question paper</p>
            {
            	this.generateForm(dataJson)
            }
            <BarChat isShowBarGraph= {this.state.showResult} resultData={this.state.resultData} value={1} resCount={this.state.resCount} totalCountQes={this.state.totalCountQes}/>
            <button type="submit" className="submit-button" title="Submit">Submit</button>
            <button type="button" className="reset" title="Clear" onClick={reset}>Clear </button>
        </form>
	 		</div>
	 		)
	 	}
  }
  function validate(values, state) {
  	// console.log('validate',values)
	}
  questionPaper = reduxForm({validate})(questionPaper);

	function mapStateToProps(state, ownProps) {
		return {
			/* set dynamic form name */
			form: "question-paper-form",

	    /* set initial values of form field */
	    initialValues: ownProps.initialVal || {}
		}
	}
export default connect(mapStateToProps, {})(questionPaper);
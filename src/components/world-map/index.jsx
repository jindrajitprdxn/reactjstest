import React, { Component } from 'react';
import { Link } from 'react-router';
import "./styles/style.css";
 class WorldMap extends Component {
 		constructor(props) {
 			super(props)
 			this.state = {
 				'cities': null,
 				'pillList': null,
 				'pillsData': []
 			}
 			this.addCities = this.addCities.bind(this)
 		}

 		addCities(e) {
 			var inputField = document.querySelector('.enter-city').value
 			let oldPillData = this.state.pillsData
 			this.setState({
 				'pillsData': oldPillData.push(inputField)
 			})
 			let pillList = oldPillData ? oldPillData.map(data => {
 				return(
 					<li><span>{data}</span></li>
 					)
 			}) : null

 			this.setState({
 				'pillList': pillList
 			})
 		}
	 	render() {
	 		let data = this.state.cities ? this.state.cities : 'mumbai'
	 		return(
	 		<div className="world-map">
	 			<h2>World Map</h2>
	 			<div>
	 				<iframe
	  				src={`https://www.google.com/maps/embed/v1/place?key=AIzaSyA9L5bK2IxF2OCM4bbSePvfPXRAQ872pwU
	   				 &q=${data}`} allowfullscreen>
					</iframe>
					<div>
						<ul>
							{this.state.pillList}
						</ul>
					</div>
					<input type="text" placeholder="Enter city name" className="enter-city" />
					<button title="Add Cities" onClick={(e) => {
						this.addCities(e)
					}}>Add City</button>
				</div>
	 		</div>
	 		)
	 	}
  }

 export default WorldMap;
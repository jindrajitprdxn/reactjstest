import React, { Component } from 'react';
import { Link } from 'react-router';
import "./style.css"
 class Home extends Component {
 	render() {
 		return(
 		<div className="parent-container">
 			<Link to="/world-map" title="World Map"> Wordl Map </Link>
 			<Link to="/question-paper" title="Question Paper"> Question Paper </Link>
 			<Link to="/articles" title="Articles"> Articles </Link>
 		</div>
 		)
 	}
 }

 export default Home;
import React, { Component } from 'react';
import './style.css';
import BarChart from 'react-bar-chart';
class BarChat extends Component {
	render() {
		const {
			isShowBarGraph,
			resultData,
			value,
			totalCountQes,
			resCount
		} = this.props;
		return (
			<div className={ isShowBarGraph ?  "bar-chat-container visible-chart" : "bar-chat-container hidden-chart"}>
			{/*<BarChat 
				height={value} 
				data={resultData ? resultData : []}
				/>*/}
			<h3 className="show-result-count">
				{` Result is  ${resCount} out of ${totalCountQes}`}
			</h3>
			<ul>
				{
					resultData.map((data, index) => {
						return (
							<li key={index}>
								<span className={data.value === 0 ? 'wrong-ans' : 'right-ans'} />
								<span className="text">{data.text}</span>
							</li>
						)
					})
				}
			</ul>
			</div>
		)
	}
}

export default BarChat
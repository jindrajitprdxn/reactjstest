import React, { Component } from 'react';
import { articleListData } from '../../actions/articles';
import { connect } from 'react-redux';
import {Button, Icon, Card} from 'react-materialize';
import { Link, browserHistory } from 'react-router';

class Articles extends Component {
	constructor(props) {
		super(props);
	}

	componentWillMount() {
		this.props.articleListData().then(res => {
		})
	}
	render() {
		const {
			isFetchingarticleListing,
			articleListingData,
			articleListingErrMsg
		} = this.props;
		return(
			<div className="articles-conatiner">
				<header>
					<div className="wrapper">
						<h1>
							<a href="index.html" title="">
								{/*<img src="assets/images/bottle.jpg" alt="">*/}
							</a>
						</h1>
						<nav className="nav-bar">
						<span className="fa fa-bars"></span>
							<ul className="menu">
								<li>
									<a href="#FIXME" title="Home" className="decoration uppercase">Home</a>
								</li>
								<li>
									<a href="#FIXME" title="About" className="decoration uppercase">About</a>
								</li>
								<li>
									<a href="#FIXME" title="Services" className="decoration uppercase">Services</a>
								</li>
							</ul>
						</nav>
					</div>
				</header>
				{ !this.props.params.id ?
				<section>
					
						<ul>
							{
								!isFetchingarticleListing && articleListingData ? articleListingData.map((data, index) => {
									return <li key={index}>
										<Card actions={[<Link to={`/articles/${data.id}`} title="Read More">Read More</Link>]}>
											<h3 className="article-title">{data.title}</h3>
											<span className="card-id">{`Article Id: ${data.id}`}</span>
											<span className="user-id">{`  Article Creater Id: ${data.userId}`}</span>
											<p className="article-body">{data.body}</p>
										</Card>
									</li>
								}) : null
							}
						</ul>
				</section> 
				:
				<div>
				{React.cloneElement(this.props.children,{
					'articleListData': !isFetchingarticleListing && articleListingData ? articleListingData : []
				})}
				</div>
				}
			</div>
		)
	}
}
function mapStateToProps(state) {
	const { 
		"articleListingReducer" : {
			isFetchingarticleListing,
			articleListingData,
			articleListingErrMsg
		}
	} = state;

	return {
		isFetchingarticleListing,
		articleListingData,
		articleListingErrMsg
	}
}
export default connect(mapStateToProps,{
 	articleListData
 })(Articles);
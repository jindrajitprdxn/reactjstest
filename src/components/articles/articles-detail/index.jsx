import React, { Component } from 'react';
import {Button, Icon, Card, CardTitle} from 'react-materialize';
import image from '../sample-1.jpg';

export default class ArticlesDetail extends Component {
	constructor(props) {
		super(props);
		this.renderArticleDetailStructure = this.renderArticleDetailStructure.bind(this);
	}

	renderArticleDetailStructure(articleListData) {
		let currentId = this.props.params.id;
		return articleListData.map(data => {
			if(data.id == currentId) {
				return <Card className='small'
			  header={<CardTitle image={image}>{data.title}</CardTitle>}
			  >
			  <p className="article-description">{data.body}</p>
			</Card>
			}
		})
	}
	render() {
		const {
			articleListData
		} = this.props;
		return(
			<div className="article-detail-container"> 
			{
				this.renderArticleDetailStructure(articleListData)
			}
			</div>
		)
	}
}
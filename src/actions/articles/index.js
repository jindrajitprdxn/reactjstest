import axios from 'axios';

import {
  ARTICLE_LISTING_REQUEST,
  ARTICLE_LISTING_SUCCESS,
  ARTICLE_LISTING_FAILURE
} from '../action-types';

export function articleListData() {
  return dispatch => {
    dispatch(articleListingReq());
    return axios.get("https://jsonplaceholder.typicode.com/posts").then(response =>{
      dispatch(articleListingSuccess(response.data));
			return response;
    }).catch(err=>{
      if(err.response) {
        dispatch(articleListingFailure(err.response.data.message));
      } else {
        // dispatch(articleListingFailure(NETWORK_FAILURE_MSG));
      }
    })
  }
}

function articleListingReq() {
  return {
    'type': ARTICLE_LISTING_REQUEST,
    isFetchingarticleListing: true
  }
}


function articleListingSuccess(articleListingData) {
  return {
    'type': ARTICLE_LISTING_SUCCESS,
    isFetchingarticleListing: false,
    articleListingData
  }
}

function articleListingFailure(articleListingErrMsg) {
  return {
    'type': ARTICLE_LISTING_FAILURE,
    isFetchingarticleListing: false,
    articleListingErrMsg
  }
}

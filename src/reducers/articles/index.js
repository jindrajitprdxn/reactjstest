import {
	ARTICLE_LISTING_REQUEST,
  ARTICLE_LISTING_SUCCESS,
  ARTICLE_LISTING_FAILURE
} from '../../actions/action-types';

const INITIAL_STATE = {
  'isFetchingarticleListing': false,
  'articleListingErrMsg': '',
  'articleListingData': null
}

export default function articleListingReducer(state = INITIAL_STATE, action) {
  switch (action.type) {
    case ARTICLE_LISTING_REQUEST:
      return Object.assign({}, state, {
        'isFetchingarticleListing': action.isFetchingarticleListing
      });
    case ARTICLE_LISTING_SUCCESS:
      return Object.assign({}, state, {
        'isFetchingarticleListing': action.isFetchingarticleListing,
        'articleListingData': action.articleListingData
      });
    case ARTICLE_LISTING_FAILURE:
      return Object.assign({}, state, {
        'isFetchingarticleListing': action.isFetchingarticleListing,
        'articleListingErrMsg': action.gridListingErrMsg
      });

    default:
      return state;
  }
}

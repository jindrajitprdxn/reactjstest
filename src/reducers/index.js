import { combineReducers, createStore, applyMiddleware } from "redux";
import ReduxThunk from 'redux-thunk';
import { reducer as formReducer } from 'redux-form';
import articleListingReducer from './articles'
const reducers = combineReducers({
		form: formReducer,
		articleListingReducer
})

export default createStore(reducers,applyMiddleware(ReduxThunk))